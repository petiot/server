﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.Models.DTO
{
    public class DeviceDto
    {
        public String Id { get; set; }
        public string MacAddress { get; set; }
        public UserDto UserOwner { get; set; }
        public AnimalDto Animal { get; set; }
    }
}
