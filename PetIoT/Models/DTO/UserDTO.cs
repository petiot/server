﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.Models.DTO
{
    public class UserDto
    {
        public String Id { get; set; }

        public String Email { get; set; }

        public String UserName { get; set; }

        public UserProfileDto UserProfile { get; set; }
    }
}
