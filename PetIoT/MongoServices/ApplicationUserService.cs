﻿using PetIoT.Models;
using PetIoT.MongoServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetIoT.Helpers;
using MongoDB.Bson;

namespace PetIoT.MongoServices
{
    public class ApplicationUserService : BaseService<ApplicationUser>, IApplicationUserService
    {
        private readonly IPetIoTCollection<ApplicationUser> _applicationUsers;

        public ApplicationUserService(IPetIoTCollection<ApplicationUser> helperEntity) 
            : base(helperEntity)
        {
            _applicationUsers = helperEntity;
        }

        /// <summary>
        /// Method responsable for returning the current logged in user.
        /// </summary>
        /// <returns>UserProfile</returns>
        public UserProfile GetLoggedInUser()
        {
            ///TODO: Really Implement this function and not mock the data.
            return new UserProfile()
            {
                _id = new ObjectId("1"),
                LastName = "Doido",
                Name = "Teste"
            }; 
        }
    }
}
