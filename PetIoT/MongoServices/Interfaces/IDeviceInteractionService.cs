﻿using PetIoT.Models;
using PetIoT.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.MongoServices.Interfaces
{
    public interface IDeviceInteractionService : IBaseService<DeviceInteraction>
    {
        bool RegisterInteraction(DeviceInteractionDto deviceInteraction);
    }
}
