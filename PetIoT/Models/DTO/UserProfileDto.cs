﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.Models
{
    public class UserProfileDto
    {
        public String Name { get; set; }
        public String LastName { get; set; }
        public String Phone { get; set; }
    }
}
