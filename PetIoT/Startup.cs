﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PetIoT.MongoServices.Interfaces;
using PetIoT.MongoServices;
using PetIoT.Helpers;
using PetIoT.Models;

namespace PetIoT
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            //Setting the configurations
            var appSettings = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettings);

            #region Dependency Inject MongoServices
            services.AddTransient<IMongoDBHelper, MongoDBHelper>();

            services.AddTransient<IPetIoTCollection<ApplicationUser>, PetIoTCollection<ApplicationUser>>();
            services.AddTransient<IApplicationUserService, ApplicationUserService>();

            services.AddTransient<IPetIoTCollection<Device>, PetIoTCollection<Device>>();
            services.AddTransient<IDeviceService, DeviceService>();

            services.AddTransient<IPetIoTCollection<DeviceInteraction>, PetIoTCollection<DeviceInteraction>>();
            services.AddTransient<IDeviceInteractionService, DeviceInteractionService>

            #endregion Dependency Inject MongoServices
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "api/{controller}/{action}/{id?}");
            });
        }
    }
}
