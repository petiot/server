﻿using MongoDB.Driver;

namespace PetIoT.Helpers
{
    public class PetIoTCollection<T> : IPetIoTCollection<T> where T : class
    {
        private readonly IMongoCollection<T> Collection;

        public PetIoTCollection(MongoDBHelper dataBaseHelper)
        {
            Collection = dataBaseHelper.DataBase.GetCollection<T>(typeof(T).Name);
        }

        public IMongoCollection<T> GetCollection()
        {
            return Collection;
        }
    }
}
