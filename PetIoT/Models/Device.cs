﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PetIoT.Models.DTO;

namespace PetIoT.Models
{
    public class Device
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public string MacAddress { get; set; }

        public UserDto UserOwner { get; set; }

        public AnimalDto Animal { get; set; }
    }
}
