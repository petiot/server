﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.Utilities
{
    public static class Util
    {
        #region CONFIGURATIONPAGING
        public static readonly int NumberOfDocumentsPerPageDefault = 10;
        public static readonly int NumberOfDevicesPerRequest = 10;
        #endregion CONFIGURATIONPAGING
    }
}
