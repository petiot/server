﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.Models
{
    public class UserProfile
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public String Name { get; set; }

        public String LastName { get; set; }
    }
}
