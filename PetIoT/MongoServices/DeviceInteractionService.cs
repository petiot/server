﻿using PetIoT.Models;
using PetIoT.MongoServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetIoT.Helpers;
using PetIoT.Models.DTO;

namespace PetIoT.MongoServices
{
    public class DeviceInteractionService : BaseService<DeviceInteraction>, IDeviceInteractionService
    {
        private readonly IPetIoTCollection<DeviceInteraction> _deviceInteractions;

        public DeviceInteractionService(IPetIoTCollection<DeviceInteraction> helperEntity) 
            : base(helperEntity)
        {
            _deviceInteractions = helperEntity;
        }

        /// <summary>
        /// This method will be called by the controller that is called by the Device to register an 
        /// interaction between the animal and the device.
        /// </summary>
        /// <param name="deviceInteraction">Data about the interaction</param>
        /// <returns>If the interaction was registered succeful or not</returns>
        public bool RegisterInteraction(DeviceInteractionDto deviceInteraction)
        {
            throw new NotImplementedException();
        }
    }
}
