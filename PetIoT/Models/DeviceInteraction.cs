﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PetIoT.Models.DTO;
using System;

namespace PetIoT.Models
{
    public class DeviceInteraction
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public DeviceDto Device { get; set; }

        public DateTime Date { get; set; }

        public double PercentageOfWaterLeft { get; set; }

        public bool UserOwnerAlerted { get; set; }
    }
}
