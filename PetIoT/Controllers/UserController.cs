using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetIoT.Filters;
using PetIoT.Models.DTO;

namespace PetIoT.Controllers
{
    public class UserController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("fdhufhudf");
        }

        [HttpGet]
        [BasicAuthorizationActionFilter]
        public IActionResult MyDevices()
        {
            List<DeviceDto> teste = new List<DeviceDto>()
            {
                new DeviceDto()
                {
                    Animal = new AnimalDto(),
                    Id = "fduhufdf",
                    MacAddress = "fhfdufh333",
                    UserOwner = new UserDto()
                }
            };

            if (teste != null)
                return Ok(teste);

            return BadRequest();
        }
    }
}