﻿using MongoDB.Bson;
using MongoDB.Driver;
using PetIoT.Helpers;
using PetIoT.MongoServices.Interfaces;
using PetIoT.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PetIoT.MongoServices
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        private readonly IPetIoTCollection<T> _entity;

        public BaseService(IPetIoTCollection<T> helperEntity)
        {
            _entity = helperEntity;
        }

        public bool Create(T entity)
        {
            try
            {
                _entity.GetCollection().InsertOne(entity);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CreateMany(List<T> entities)
        {
            try
            {
                _entity.GetCollection().InsertMany(entities);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Delete(FilterDefinition<T> filter)
        {
            try
            {
                var update = Builders<T>.Update
                .Set("isDeleted", "S");

                _entity.GetCollection().UpdateOne(filter, update);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public T GetById(ObjectId id)
        {
            //TODO Change this method to make an async call?
            var builder = Builders<T>.Filter;
            var filter = builder.Eq("_id", id);
            return _entity.GetCollection().Find(filter).FirstOrDefault();
        }

        public IEnumerable<T> GetAllByFilter(FilterDefinition<T> filter, int? currentPage, int? take)
        {
            try
            {
                if (filter == null)
                    filter = Builders<T>.Filter.Empty;

                if (!currentPage.HasValue || currentPage.Value < 0)
                    currentPage = 0;

                if (!take.HasValue || take.Value <= 0)
                    take = Util.NumberOfDocumentsPerPageDefault;

                return _entity.GetCollection().Find(filter).Skip(currentPage * take).Limit(take).ToEnumerable<T>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(FilterDefinition<T> filter, UpdateDefinition<T> update)
        {
            try
            {
                /*
                Example of UpdateDefinition that can be use for the PetIoT Project.
                var update = Builders<Equipe>.Update.Set("Nome", equipeToUpdate.Nome)
                                    .Set(up => up.Descricao, equipeToUpdate.Descricao);
                */
                _entity.GetCollection().UpdateOne(filter, update);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateAllFieldsOfOneDocument(FilterDefinition<T> filter, T newEntity)
        {
            try
            {
                var update = _entity.GetCollection().ReplaceOne(filter, newEntity);
                return update.IsAcknowledged == true && update.ModifiedCount == 1 ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
