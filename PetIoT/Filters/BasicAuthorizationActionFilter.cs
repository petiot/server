﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PetIoT.MongoServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetIoT.Filters
{
    public class BasicAuthorizationActionFilter : TypeFilterAttribute
    {
        public BasicAuthorizationActionFilter()
                : base(typeof(BasicAuthorizationActionFilterImpl))
        {
        }

        private class BasicAuthorizationActionFilterImpl : IAsyncActionFilter
        {
            private readonly IApplicationUserService _applicationUserService;
            public BasicAuthorizationActionFilterImpl(IApplicationUserService applicationUserService)
            {
                _applicationUserService = applicationUserService;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                string authHeader = context.HttpContext.Request.Headers["Authorization"];
                if (authHeader != null && authHeader.StartsWith("Basic"))
                {
                    var token = authHeader.Substring("Basic ".Length).Trim();
                    var credentialString = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                    var credentials = credentialString.Split(':');

                    var userName = credentials[0];
                    var password = credentials[1];

                    if (UserLoggedIn(userName, password))
                    {
                        //Set the current user to this user.
                    }
                    else
                    {
                        context.Result = new UnauthorizedResult();
                        return;
                    }
                }
                else
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }

                await next();
            }

            public bool UserLoggedIn(string userName, string password)
            {
                if(userName == "test" && password == "test")
                    return true;

                return false;
            }
        }
    }
}
