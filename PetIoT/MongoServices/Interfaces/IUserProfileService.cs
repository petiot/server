﻿using PetIoT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.MongoServices.Interfaces
{
    public interface IUserProfileService : IBaseService<UserProfile>
    {
    }
}
