﻿using MongoDB.Driver;

namespace PetIoT.Helpers
{
    public interface IPetIoTCollection<T>
    {
        IMongoCollection<T> GetCollection();
    }
}
