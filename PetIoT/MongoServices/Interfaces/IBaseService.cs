﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT.MongoServices.Interfaces
{
    public interface IBaseService<T> where T : class
    {
        bool Create(T entity);

        bool CreateMany(List<T> entities);

        bool Delete(FilterDefinition<T> filter);

        bool Update(FilterDefinition<T> filter, UpdateDefinition<T> update);

        bool UpdateAllFieldsOfOneDocument(FilterDefinition<T> filter, T newEntity);

        T GetById(ObjectId id);

        IEnumerable<T> GetAllByFilter(FilterDefinition<T> filter, int? skip, int? take);
    }
}
