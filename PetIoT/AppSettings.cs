﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetIoT
{
    public class AppSettings
    {
        public String MongoDBServerAddress { get; set; }

        public String MongoDBDatabaseName { get; set; }
    }
}
