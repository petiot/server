﻿using MongoDB.Bson;
using MongoDB.Driver;
using PetIoT.Helpers;
using PetIoT.Models;
using PetIoT.MongoServices.Interfaces;
using PetIoT.Utilities;
using System;
using System.Collections.Generic;

namespace PetIoT.MongoServices
{
    public class DeviceService : BaseService<Device>, IDeviceService
    {
        private readonly IPetIoTCollection<Device> _devices;
        private readonly IApplicationUserService _applicationUserService;

        public DeviceService(IPetIoTCollection<Device> helperEntity,
                             IApplicationUserService applicationUserService) 
            : base(helperEntity)
        {
            _devices = helperEntity;
            _applicationUserService = applicationUserService;
        }

        public IEnumerable<Device> GetAllMyDevices(int? page)
        {
            try
            {
                string userOwnerId = _applicationUserService.GetLoggedInUser()._id.ToString();

                var builders = Builders<Device>.Filter;
                var filter = builders.Where(x => x.UserOwner.Id == userOwnerId);
                return GetAllByFilter(filter, page, Util.NumberOfDevicesPerRequest);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
